import React from "react";
import { dataGlasses } from "./dataGlasses";

export default function GlassesList({ handleChangeGlasses }) {
  let handleRenderGlassList = () => {
    return dataGlasses.map((glasses, key) => {
      return (
        <div className="col-2" key={key}>
          <img
            style={{ width: "100%", cursor: "pointer" }}
            src={glasses.url}
            alt=""
            onClick={() => {
              handleChangeGlasses(glasses);
            }}
          />
        </div>
      );
    });
  };
  return <div className="row bg-light p-3 mt-4">{handleRenderGlassList()}</div>;
}
