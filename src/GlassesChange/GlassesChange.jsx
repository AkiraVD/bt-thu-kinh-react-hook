import React, { useEffect, useState } from "react";
import GlassesList from "./GlassesList";
import Header from "./Header";

export default function GlassesChange() {
  let [glasses, setGlasses] = useState(0);
  console.log("glasses: ", glasses);
  let handleChangeGlasses = (data) => {
    setGlasses(data);
  };
  let display$ = glasses == 0 ? "none" : "block";
  return (
    <div
      style={{
        backgroundColor: "#FF9A8B",
        backgroundImage:
          "linear-gradient(90deg, #FF9A8B 0%, #FF6A88 55%, #FF99AC 100%)",
        height: "100vh",
      }}
    >
      <div className="mb-4" style={{ backgroundColor: "rgba(156,75,124,0.8)" }}>
        <Header />
      </div>
      <div className="container">
        <div
          className="mx-auto"
          style={{
            height: "400px",
            width: "max-content",
            position: "relative",
          }}
        >
          <img
            style={{ height: "100%" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
          <div
            style={{
              position: "absolute",
              top: "25%",
              left: "0",
              right: "0",
            }}
          >
            <img
              src={glasses.url}
              alt=""
              style={{ width: "60%", opacity: "80%" }}
            />
          </div>
          <div
            className="bg-secondary text-white p-2 text-start bg-opacity-75"
            style={{
              position: "absolute",
              bottom: "0",
              right: "0",
              left: "0",
              display: display$,
            }}
          >
            <h5 className="text-danger">{glasses.name}</h5>
            <span className="text-warning text-end">
              {"$" + glasses.price}
            </span>{" "}
            <br />
            <span>{glasses.desc}</span>
          </div>
        </div>
        <GlassesList handleChangeGlasses={handleChangeGlasses} />
      </div>
    </div>
  );
}
